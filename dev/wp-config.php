<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'intcomex-little');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'nZJJvN!&PY|X@s06T@Ed(lr-quGThC*5mcUmfP397F?Tyl@2m_U:jhWPxR6[h!%y');
define('SECURE_AUTH_KEY',  'I%qw[S0$SFa 6ZL+6,9p;Vdz$r1=4/RcXv^ZY`+V.dDv;eQSL%Dx0TcZ.Qu9KI_}');
define('LOGGED_IN_KEY',    '=xoE>eBQMPt=I^[0|up3rK?[v|Z->VwJ?+CB`Ko0G-DhF#t1f}4Ad>4oAz/S0qsV');
define('NONCE_KEY',        'J[$P||fupb}gC1ECFawK8J>!x+B]4Uo$,.~wruOX5>&`vL8Q!cU8dVt/Z~/2#Fb/');
define('AUTH_SALT',        'n(><LCbJ%5:c%m}Y3z-(|]~E]A%=|PH=oOPV6e<qK&(F<-sQj(~1/N?NYd+?K%(h');
define('SECURE_AUTH_SALT', 'KQY,cnfzZ+eLV^Yna+IDka~~K4/,Dg(ihj/2Dq4,8sCo+v){23<|Z4,y5^{XoAL~');
define('LOGGED_IN_SALT',   'txYnXwP,3:[$XHg5LLdu-ew;<9Hk&%E#79e>}N^DTo*|P{`Lno~j}dbOu^z0_/7I');
define('NONCE_SALT',       '=!IT^1tvR8S})@;%Y>@AnVSBS0:gXG?K^bnTs?!5W~S0Xar}(WR!X]&L0V4|AL>A');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wplt_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
