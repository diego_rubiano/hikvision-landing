<?php
$my_url;
/**
LLAMADO LIBRERIA CMB
*/
if ( file_exists( dirname( __FILE__ ) . '/lib/cmb2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/lib/cmb2/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/lib/CMB2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/lib/CMB2/init.php';
}
/**
OPCIONES DEL TEMA 
**/
require_once ( get_template_directory() . '/admin/theme-options.php' );

/**
LOGO PERSONALIZADO EN EL ADMINISTRADOR 
**/
function my_login_logo() { ?>
    <style type="text/css">
        body.login div#login h1 a {
			background-image: url(<?php echo theme_get_option( 'logo' ); ?>);
			padding-bottom: 0px;
			width: 100%;
			height: 90px;
			background-size: initial;
        }
	    body.login div#login form#loginform p.submit input#wp-submit {
			background-color: #006242;
			border: solid 1px #006242;
			color: #FFF;
			outline: none;
			box-shadow: none;
		}
    </style>
<?php
}
add_action( 'login_enqueue_scripts', 'my_login_logo' );
function my_login_logo_url_title() {
    return 'Hickvision';
}
/**
SOPORTE PARA IMAGENES EN EL POST Y REMOVER LOS ATRIBUTOS WIDHT & HEIGHT
**/
if (function_exists('add_theme_support')) { 
	add_theme_support('post-thumbnails');
}
add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );
function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}
/**
	Registro Menus
**/
function register_my_menus() {
  register_nav_menus(
    array(
      'main-menu' 	=> 'Main Menu',
      'footer-menu'	=> 'Menú Footer',
      'social-menu' => 'Menú Redes sociales'
    )
  );
}
add_action( 'init', 'register_my_menus' );

/**
	Excerpt Page 
**/
add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

/**
get post options
*/
function cmb2_get_post_options( $query_args ) {

    $args = wp_parse_args( $query_args, array(
        'post_type'   => 'post',
        'numberposts' => 10,
    ) );

    $posts = get_posts( $args );

    $post_options = array();
    if ( $posts ) {
        foreach ( $posts as $post ) {
          $post_options[ $post->post_name ] = $post->post_title;
        }
    }

    return $post_options;
}
function cmb2_get_post_options_by_id( $query_args ) {

    $args = wp_parse_args( $query_args, array(
        'post_type'   => 'post',
        'numberposts' => 10,
    ) );

    $posts = get_posts( $args );

    $post_options = array();
    if ( $posts ) {
        foreach ( $posts as $post ) {
          $post_options_id[ $post->ID ] = $post->post_title;
        }
    }

    return $post_options_id;
}

/**
    Admin Inspiration option
*/
function mytheme_admin_load_scripts($hook) {
    if( $hook != 'post.php' && $hook != 'post-new.php') 
        return;
    wp_enqueue_script( 'custom-js', get_template_directory_uri()."/js/option-admin.js" );
}
add_action('admin_enqueue_scripts', 'mytheme_admin_load_scripts');
/**
quitar string de [...]
*/
function new_excerpt_more( $more ) {
    return '';
}
add_filter('excerpt_more', 'new_excerpt_more');
?>