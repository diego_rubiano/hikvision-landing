        </div><!-- general-wrapper -->
        <div id="footer" class="row">
            <div class="top container">
                <nav class="footer-menu">
                    <?php wp_nav_menu(array('theme_location' => 'footer-menu', 'container' => false));?>
                </nav>
            </div>
        </div><!-- End footer-->	
        <?php wp_footer(); ?>
    </body>
</html>