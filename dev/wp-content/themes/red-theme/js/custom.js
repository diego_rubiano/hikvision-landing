$(document).ready(function () {
    
    /*------ MODAL CONTENT FUNCTIONS -------*/
    $('a.mycontent').on('click',function(){
        var cont = $(this).attr('data-content');
        var cargo = $(this).attr('data-cargo');
        console.log('cargo: ',cargo);
        console.log('contenido: ',cont);
        var contenido = '<p class="col-xs-12 col-sm-12 col-md-12">'+cont+'</p>';
        console.log('append contenido: ',contenido);
        var title = $(this).attr('data-title');
        var mTitle = title+'<br><span>'+cargo+'</span>';
        $('#myModal').modal();
        $('#myModal').on('shown.bs.modal', function(){
            $('#myModal .modal-title').html(mTitle);
            $('#myModal .modal-body').html(contenido);
        });
        $('#myModal').on('hidden.bs.modal', function(){
            $('#myModal .modal-body').html('');
        });
    });

    /*------ END OF MODAL CONTENT FUNCTIONS -------*/
    /*------ BX SLIDER FUNCTIONS -------*/
    /*------ BX SLIDER ALLIANCES -------*/
    var options;
    options = {
        mode: 'horizontal',
        infiniteLoop: true,
        speed: 500,
        pause: 3000,
        auto: true,
        pager: false,
        controls: true,
        nextText:'',
        prevText:'',
        useCSS: false,
        randomStart: false,
    }

    /*------ BX SLIDER  events -------*/
    $('#slidebx').bxSlider(options);
    var options2;
    options2 = {
        mode: 'horizontal',
        infiniteLoop: true,
        speed: 3000,
        pause: 3000,
        auto: true,
        pager: false,
        controls: true,
        nextText:'',
        randomStart: true,
        prevText:''
    }
    $('.carousel-gallery').bxSlider(options2);
    /*------ END BX SLIDER FUNCTIONS -------*/
    /*---Before change content functions----*/
    $('.facebook a').first().html("<i class='fa fa-facebook-square'></i> ").css({"font-size":"30px", "color":"blue"});
    $('.youtube a').first().html("<i class='fa fa-youtube-square'></i> ").css({"font-size":"30px", "color":"red"});
    $('.twitter a').first().html("<i class='fa fa-twitter-square'></i> ").css({"font-size":"30px", "color":"cyan"});
    $('.linkedin a').first().html("<i class='fa fa-linkedin-square'></i> ").css({"font-size":"30px", "color":"blue"});
    $('.instagram a').first().html("<i class='fa fa-instagram'></i> ").css({"font-size":"30px", "color":"white"});
    /*--End Before change content functions-*/

    //var waypoint = new Waypoint({
     // element: document.getElementById('main'),
     // handler: function(direction) {
      //  $('.navbar-icp').toggleClass('menu-fixed', direction === 'down');
     // },
      //offset: 100 
    //});
});
