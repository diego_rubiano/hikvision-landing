(function($){
    $(document).ready(function(){
        $('#postimagediv .inside').prepend('<p>para las secciones de publicaciones y eventos por favor cargar imágenes de 820px x 500px, para las secciones de los perfiles (liderazgo, expertos), cargar imágenes de 500px x 500px, para la sección de aliados, cargar imágenes de 250px x 120px </p>');
        $("#mcmd_metabox-subareas").hide();
        //lets add the interactivity by adding an event listener
        $("#mcmd_areas-taxonomy").bind("change",function(){
            if ($(this).val()=='analisis-internacional'){
                // photographer
                $("#mcmd_metabox-subareas").show();
            } else {
                //still confused, hasn't selected any
                $("#mcmd_metabox-subareas").hide();
            }
        });
        //make sure that these metaboxes appear properly in profession edit screen
        if($("#mcmd_areas-taxonomy4").is(':checked')){ //photographer
            $("#mcmd_metabox-subareas").show();}
    })
})(jQuery);