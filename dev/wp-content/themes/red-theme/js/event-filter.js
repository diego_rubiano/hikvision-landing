jQuery(document).ready(function($) {
    $(".opcion").click(function(){
    	$('.opcion').not(this).removeClass("selected");
    	$(this).toggleClass("selected");
    	var categoria = $(".selected").attr('data-value');
    	console.log(categoria);
		jQuery.post(MyAjax.url, {nonce : MyAjax.nonce, action : 'search_post_event', cat : categoria }, 
		function(response) {
			if (categoria == 'null_cat'){
    			location.reload(true);
	    	}else{
		    	$('#wrapper-eventos').hide().html(response);
		    	$('#wrapper-eventos').fadeIn();
		    	$('#reset').show();
		    }
		});
	});
});
