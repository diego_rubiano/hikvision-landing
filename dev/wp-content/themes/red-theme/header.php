<!doctype html>
<html <?php language_attributes( $doctype ) ?>>
<head>
    <meta charset="UTF-8">
    <title><?php wp_title(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--FAVICON-->
    <link rel="icon" href="<?php echo theme_get_option( 'favicon' ); ?>" type="image/x-icon">
    <!-- APPLE TOUCH ICON-->
    <link rel="apple-touch-icon-precomposed" href="<?php echo theme_get_option( 'apple_icon_57' ); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo theme_get_option( 'apple_icon_72' ); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo theme_get_option( 'apple_icon_114' ); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo theme_get_option( 'apple_icon_144' ); ?>">
    <!-- ESTILOS-->
    <link href="<?php bloginfo('stylesheet_directory'); ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php bloginfo('stylesheet_directory'); ?>/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/lib/bxslider/jquery.bxslider.css">
    <!-- JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/bootstrap.min.js"></script>
    <!-- bxSlider Javascript file -->
    <script src="<?php bloginfo('stylesheet_directory'); ?>/lib/bxslider/jquery.bxslider.js"></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.waypoints.min.js"></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/sticky.min.js"></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/custom.js"></script>  
     
    <!-- bxSlider CSS file -->
    <link href="<?php bloginfo('stylesheet_directory'); ?>/lib/bxslider/jquery.bxslider.css" rel="stylesheet" />
    <?php wp_head();?>

</head>
<body <?php body_class( 'container-fluid' ); ?>>
  <header class="main-header">
      
  </header>
<div class="general-wrapper" id="main">