<?php
/*
Plugin Name: Metabox Plugin
Description: Crea los metabox para el sitio
Version: 0.1
Author: Dynamik Collective
Author URI: http://www.dynamikcollective.com/
*/
function cmb2_get_term_options() {
    $taxonomy = 'categoria-areas';
    $args = array( 
        'post_type' => 'areas',
        'orderby' => 'id',
        'hide_empty' => 0,
        'posts_per_page' => -1
    );
    $cat_areas = get_terms($taxonomy,$args);
    $term_options = array();
    if (!empty ($cat_areas)){
        foreach ($cat_areas as $categoria_areas) {
            $termchildren = get_term_children($categoria_areas->term_id, $taxonomy);
            if ($termchildren){
                $term_options[$categoria_areas->slug] = $categoria_areas->name;                
            }
        }
    }
    return $term_options;
}
add_action( 'cmb2_init', 'yourprefix_register_demo_metabox' );
/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_init' hook.
 */
function yourprefix_register_demo_metabox() {

	// Start with an underscore to hide fields from custom fields list
	$prefix = 'mcmd_';

	/**
	 * Sample metabox to demonstrate each field type included
	 */
	/* Liderazgo */
    $publicaciones = new_cmb2_box( array(
        'id'            => $prefix . 'metabox-publicaciones',
        'title'         => __( 'Detalles de la publicacion', 'metabox-dynamik' ),
        'object_types'  => array( 'post', ), // Post type
        'context'       => 'normal',
        'priority'      => 'default',
        'show_names'    => true, // Show field names on the left
    ) );
        $publicaciones->add_field( array(
            'name' => __( 'Autor: ', 'metabox-dynamik' ),
            'id'   => $prefix . 'autor-publicaciones',
            'type'    => 'text'
        ) );
    $publicaciones2 = new_cmb2_box( array(
        'id' => $prefix.'metabox-publicaciones2',
        'title' => __('Destacar en home', 'metabox-dynamik'),
        'object_types' => array('post',),
        'context' => 'side',
        'priority' => 'default',
        'show_names' => false,
    ));
        $publicaciones2->add_field( array(
            'name' => __( 'publicacion destacada: ', 'metabox-dynamik' ),
            'id'   => $prefix . 'publicacion-destacada',
            'desc' => __( 'seleccione esta opcion para destacar la publicacion en el home', 'metabox-dynamik' ),
            'type'    => 'checkbox'
        ) );
	$liderazgo = new_cmb2_box( array(
		'id'            => $prefix . 'metabox-liderazgo',
		'title'         => __( 'Detalles del miembro', 'metabox-dynamik' ),
		'object_types'  => array( 'liderazgo', ), // Post type
		'context'       => 'side',
		'priority'      => 'default',
		'show_names'    => true, // Show field names on the left
	) );
       $liderazgo->add_field( array(
            'name' => __( 'Cargo', 'metabox-dynamik' ),
            'id'   => $prefix . 'cargo-liderazgo',
            'type'    => 'text'
        ) );
        $liderazgo->add_field( array(
            'name' => __( 'Descripcion global del cargo', 'metabox-dynamik' ),
            'id'   => $prefix . 'cargo2-liderazgo',
            'type'    => 'select',
            'show_option_none' => true,
            'default' => 'none',
            'options' => array(
                'direccion-ejecutiva' => 'Dirección Ejecutiva',
                'coordinacion-proyectos' => 'Coordinación de Proyectos',
                'coordinacion-administrativa' => 'Coordinación Administrativa y Financiera',
                'servicios-generales' => 'Servicios Generales',
                )
        ) );
        $liderazgo2 = new_cmb2_box( array(
		'id'            => $prefix . 'metabox-liderazgo2',
		'title'         => __( 'Criterio de ordenamiento', 'metabox-dynamik' ),
		'object_types'  => array( 'liderazgo', ), // Post type
		'context'       => 'side',
		'priority'      => 'default',
		'show_names'    => true, // Show field names on the left
	) );
        $liderazgo2->add_field( array(
			'name' => __( 'Apellido', 'metabox-dynamik' ),
			'id'   => $prefix . 'apellido-liderazgo',
            'desc' => __( 'para ordenar los miembros, use este campo ingresando el apellido del miembro, si el miembro ya falleció ponga una z- antes del apellido.', 'metabox-dynamik' ),
			'type'    => 'text'
		) );
    $redes = new_cmb2_box( array(
    'id'            => $prefix . 'metabox-redes',
    'title'         => __( 'Vínculo al sitio', 'metabox-dynamik' ),
    'object_types'  => array( 'redes', ), // Post type
    'context'       => 'side',
    'priority'      => 'default',
    'show_names'    => true, // Show field names on the left
    ) );
        $redes->add_field( array(
                'name' => __( 'Link', 'metabox-dynamik' ),
                'id'   => $prefix . 'link-redes',
                'type'    => 'text_url'
            ) );
    $hitos = new_cmb2_box( array(
    'id'            => $prefix . 'metabox-hitos',
    'title'         => __( 'Fecha Hito', 'metabox-dynamik' ),
    'object_types'  => array( 'hitos', ), // Post type
    'context'       => 'side',
    'priority'      => 'default',
    'show_names'    => true, // Show field names on the left
    ) );
        $hitos->add_field( array(
                'name' => __( 'Seleccione la fecha', 'metabox-dynamik' ),
                'id'   => $prefix . 'fecha-hitos',
                'type'    => 'text_date_timestamp'
            ) );
    $medios = new_cmb2_box( array(
    'id'            => $prefix . 'metabox-medios',
    'title'         => __( 'Información adicional del artículo', 'metabox-dynamik' ),
    'object_types'  => array( 'medios', ), // Post type
    'context'       => 'side',
    'priority'      => 'default',
    'show_names'    => true, // Show field names on the left
    ) );
        $medios->add_field( array(
                'name' => __( 'Subtitulo del artículo', 'metabox-dynamik' ),
                'id'   => $prefix . 'subtitulo-medios',
                'type'    => 'text'
            ) );
        $medios->add_field( array(
                'name' => __( 'Enlace del artículo', 'metabox-dynamik' ),
                'id'   => $prefix . 'link-medios',
                'type'    => 'text_url'
            ) );
        $medios->add_field( array(
                'name' => __( 'Autor del artículo', 'metabox-dynamik' ),
                'id'   => $prefix . 'autor-medios',
                'type'    => 'text'
            ) );
    $areas = new_cmb2_box( array(
    'id'            => $prefix . 'metabox-areas',
    'title'         => __( 'Información adicional de la  publicación: ', 'metabox-dynamik' ),
    'object_types'  => array( 'areas', ), // Post type
    'context'       => 'side',
    'priority'      => 'default',
    'show_names'    => true, // Show field names on the left
    ) );
        $areas->add_field( array(
                'name' => __( 'Video', 'metabox-dynamik' ),
                'desc' => __( 'llenar este campo solo si la publicacion pertenece a la subcategoria videos', 'metabox-dynamik' ),
                'id'   => $prefix . 'video-areas',
                'type'    => 'text_url'
            ) );
        $areas->add_field( array(
                'name' => __( 'Autor', 'metabox-dynamik' ),
                'desc' => __( 'Autor del articulo', 'metabox-dynamik' ),
                'id'   => $prefix . 'autor-areas',
                'type'    => 'text'
            ) );
    $selector = new_cmb2_box( array(
        'id'            => $prefix . 'metabox-selector-areas',
        'title'         => __( 'Áreas programáticas', 'dynamik-plugin' ),
        'object_types'  => array( 'eventos', 'post'), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true,
        'fields'        => array(
            array(
                'name' => __( 'Categoría', 'metabox-dynamik' ),
                'desc' => 'Selecciona la categoria de áreas programáticas a la cual pertenece esta publicación',
                'id' => $prefix . 'areas-taxonomy',
                'type' => 'multicheck',
                'options' => cmb2_get_term_options()
            ),
        ),
    ) );
    $selectorsubareas = new_cmb2_box( array(
    'id'            => $prefix . 'metabox-subareas',
    'title'         => __( 'Categoría hija de áreas programáticas', 'metabox-dynamik' ),
    'object_types'  => array('post' ), // Post type
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    ) );
    $selectorsubareas->add_field( array(
        'name' => __('subcategoría','metabox-dynamik'),
        'id' => $prefix.'subareas-taxonomy',
        'show_option_none' => true,
        'default' => 'Ninguno',
        'type' => 'select',
        'show_names'    => true,
        'options' => array(
            'observatorio-de-fronteras' => 'Observatorio de fronteras',
            'observatorio-de-politica-y-estrategia-en-america-latina' => 'Observatorio de política y estrategia en América Latina',
        ),
    ));
    $eventos = new_cmb2_box( array(
    'id'            => $prefix . 'metabox-eventos',
    'title'         => __( 'Información adicional del evento: ', 'metabox-dynamik' ),
    'object_types'  => array( 'eventos', ), // Post type
    'context'       => 'side',
    'priority'      => 'default',
    'show_names'    => true, // Show field names on the left
    ) );
        $eventos->add_field( array(
                'name' => __( 'Ciudad', 'metabox-dynamik' ),
                'desc' => __( 'Ciudad del evento', 'metabox-dynamik' ),
                'id'   => $prefix . 'ciudad-eventos',
                'type'    => 'text'
            ) );
        $eventos->add_field( array(
                'name' => __( 'Lugar', 'metabox-dynamik' ),
                'desc' => __( 'Lugar del evento', 'metabox-dynamik' ),
                'id'   => $prefix . 'lugar-eventos',
                'type'    => 'text'
            ) );
        $eventos->add_field( array(
                'name' => __( 'Anfitrion', 'metabox-dynamik' ),
                'desc' => __( 'Anfitrion del evento', 'metabox-dynamik' ),
                'id'   => $prefix . 'anfitrion-eventos',
                'type'    => 'text'
            ) );
        $eventos->add_field( array(
                'name' => __( 'Seleccione la fecha de inicio del evento', 'metabox-dynamik' ),
                'id'   => $prefix . 'fecha-eventos-1',
                'type'    => 'text_date_timestamp'
            ) );
        $eventos->add_field( array(
                'name' => __( 'Seleccione la fecha de finalización del evento', 'metabox-dynamik' ),
                'id'   => $prefix . 'fecha-eventos-2',
                'type'    => 'text_date_timestamp'
            ) );
        $eventos->add_field( array(
                'name' => __( 'Documento descargable', 'metabox-dynamik' ),
                'desc' => __( 'Documento en formato PDF', 'metabox-dynamik' ),
                'id'   => $prefix . 'desc-eventos',
                'type'    => 'file'
            ) );
    $eventos4 = new_cmb2_box( array(
        'id'            => $prefix . 'metabox-eventos4',
        'title'         => __( 'Resumen del evento', 'metabox-dynamik' ),
        'object_types'  => array( 'eventos', ), // Post type
        'context'       => 'normal',
        'priority'      => 'default',
        'show_names'    => false, // Show field names on the left
    ) );
        $eventos4->add_field( array(
            'name' => __( 'descripcion:', 'metabox-dynamik' ),
            'id'   => $prefix . 'desc-evento',
            'type'    => 'wysiwyg',
        ) );
    $eventos5 = new_cmb2_box( array(
        'id'            => $prefix . 'metabox-eventos5',
        'title'         => __( 'Galería del evento', 'metabox-dynamik' ),
        'object_types'  => array( 'eventos', ), // Post type
        'context'       => 'normal',
        'priority'      => 'default',
        'show_names'    => false, // Show field names on the left
    ) );
        $eventos5->add_field( array(
            'name' => __( 'galeria:', 'metabox-dynamik' ),
            'id'   => $prefix . 'galeria-evento',
            'type'    => 'file_list',
        ) );
    $estado = new_cmb2_box( array(
    'id'            => $prefix . 'metabox-estado',
    'title'         => __( 'Información adicional del estado del proyecto: ', 'metabox-dynamik' ),
    'object_types'  => array( 'estado', ), // Post type
    'context'       => 'normal',
    'priority'      => 'default',
    'show_names'    => true, // Show field names on the left
    ) );
        $estado->add_field( array(
                    'name' => __( 'Proyecto al que pertenece', 'metabox-dynamik' ),
                    'desc' => __( 'Proyecto al que pertenece', 'metabox-dynamik' ),
                    'id'   => $prefix . 'proyecto-estado',
                    'show_option_none' => true,
                    'default'          => 'Ninguno',
                    'type'    => 'select',
                    'options' => cmb2_get_post_options( array( 'post_type' => 'monitoreo', 'numberposts' => -1 ) )
                ) );
        $estado->add_field( array(
                    'name' => __( 'Artículo relacionado', 'metabox-dynamik' ),
                    'desc' => __( 'Artículo relacionado', 'metabox-dynamik' ),
                    'id'   => $prefix . 'articulo-estado',
                    'show_option_none' => true,
                    'default'          => 'Ninguno',
                    'type'    => 'select',
                    'options' => cmb2_get_post_options_by_id( array( 'post_type' => 'post', 'numberposts' => -1 ) )
                ) );
        $estado->add_field( array(
                    'name' => __( 'Video relacionado', 'metabox-dynamik' ),
                    'desc' => __( 'Video relacionado', 'metabox-dynamik' ),
                    'id'   => $prefix . 'video-estado',
                    'show_option_none' => true,
                    'default'          => 'Ninguno',
                    'type'    => 'select',
                    'options' => cmb2_get_post_options_by_id( array( 'post_type' => 'areas', 'numberposts' => -1 ) )
                ) );
        $estado->add_field( array(
                    'name' => __( 'Seleccione la fecha de radicado del estado', 'metabox-dynamik' ),
                    'id'   => $prefix . 'radicado-estado',
                    'type'    => 'text_date_timestamp'
                ) );
    $aliados = new_cmb2_box( array(
    'id'            => $prefix . 'metabox-aliados',
    'title'         => __( 'Pagina web aliado: ', 'metabox-dynamik' ),
    'object_types'  => array( 'aliados', ), // Post type
    'context'       => 'normal',
    'priority'      => 'default',
    'show_names'    => false, // Show field names on the left
    ) );
        $aliados->add_field( array(
                    'name' => __( 'web', 'metabox-dynamik' ),
                    'id'   => $prefix . 'link-aliados',
                    'type'    => 'text_url'
                ) );
    

}
/**** METABOX TAXONOMY  *****/
function cmb2_taxonomy_meta_initiate() {

    //require_once( 'CMB2/init.php' );
    require_once(get_template_directory() . '/lib/CMB2/init.php');
    require_once( 'Taxonomy_MetaData/Taxonomy_MetaData_CMB2.php' );

    /**
     * Semi-standard CMB2 metabox/fields array
     */
    $meta_box = array(
        'id'         => 'cat_options',
        // 'key' and 'value' should be exactly as follows
        'show_on'    => array( 'key' => 'options-page', 'value' => array( 'unknown', ), ),
        'show_names' => true, // Show field names on the left
        //'name' => __( 'Configuración Extra Categorías de productos', 'taxonomy-metadata' ),
        'fields'     => array(
            array(
                'name' => __( 'Imagen de la categoría', 'taxonomy-metadata' ),
                'id'   => 'file_category_produc',
                'type' => 'file',
            ),
        )
    );
    // (Recommended) Use wp-large-options
    require_once( 'wp-large-options/wp-large-options.php' );
    $overrides = array(
        'get_option'    => 'wlo_get_option',
        'update_option' => 'wlo_update_option',
        'delete_option' => 'wlo_delete_option',
    );

    /**
     * Instantiate our taxonomy meta class
     */
    $cats = new Taxonomy_MetaData_CMB2( 'categoria-liderazgo', $meta_box, __( 'Nombre del miembro del consejo', 'taxonomy-metadata' ), $overrides );
}
cmb2_taxonomy_meta_initiate();